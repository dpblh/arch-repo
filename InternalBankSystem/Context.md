```plantuml
@startuml

!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
!include https://gitlab.com/dpblh/arch-repo/-/raw/master/registry.puml

LAYOUT_WITH_LEGEND()

title System Context diagram for Internet Banking System

Person(customer, Customer, "A customer of the bank, with personal bank accounts")

System(banking_system, "Internet Banking System", "Allows customers to view information about their bank accounts, and make payments.")

EMailSystem_Ext(mail_system)
MainframeSystem_Ext(mainframe)


Rel(customer, banking_system, "Uses")
Rel_Back(customer, mail_system, "Sends e-mails to")
Rel_Neighbor(banking_system, mail_system, "Sends e-mails", "SMTP")
Rel(banking_system, mainframe, "Uses")
@enduml

```
#### Hot links

1. [Internal Bank System](./Container.md)
2. [E-mail system](../EMailSystem/Context.md)
3. [Mainframe Banking System](../MainframeSystem/Context.md)
