## Mainframe Bank System

### Teams

| Role        | People           |
| ------------- |-------------|
| Managers      | @user1 |
| Developers      | @user2 @user3      |
| Supports | @user4      |

### Description system

C4-PlantUML combines the benefits of PlantUML and the C4 model for providing a simple way of describing and communicate software architectures – especially during up-front design sessions – with an intuitive language using open source and platform independent tools.
[Context Diagram](./Context.md)

