```plantuml
@startuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
!include https://gitlab.com/dpblh/arch-repo/-/raw/master/registry.puml

title System Context diagram for Mainframe Bank System

MainframeSystem(mainframe)

@enduml
```
